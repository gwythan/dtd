# HTML5 DTD #

## Licence ##

Portions copyright © 2015 W3C® (MIT, ERCIM, Keio, Beihang). This software or document includes material copied from or derived from HTML5: W3C Recommendation 28 October 2014 https://www.w3.org/TR/2014/REC-html5-20141028/.

Disclaimers (W3C): THIS DOCUMENT IS PROVIDED "AS IS," AND COPYRIGHT HOLDERS MAKE NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT, OR TITLE; THAT THE CONTENTS OF THE DOCUMENT ARE SUITABLE FOR ANY PURPOSE; NOR THAT THE IMPLEMENTATION OF SUCH CONTENTS WILL NOT INFRINGE ANY THIRD PARTY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER RIGHTS. COPYRIGHT HOLDERS WILL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF ANY USE OF THE DOCUMENT OR THE PERFORMANCE OR IMPLEMENTATION OF THE CONTENTS THEREOF. The name and trademarks of copyright holders may NOT be used in advertising or publicity pertaining to this document or its contents without specific, written prior permission. Title to copyright in this document will at all times remain with copyright holders.

HTML5 DTD Derivative Works from the W3C original works copyright 2015-2016 By Gwythan Ltd
Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.

You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.

## Introduction ##

To properly work with XHTML documents using PHP's DOMDocument class you need to supply a valid DTD. Unfortunately, there is no DTD for XHTML5, so I created one.

I have built the HTML5 DTD using the final spec at [http://www.w3.org/TR/2014/REC-html5-20141028/](http://www.w3.org/TR/2014/REC-html5-20141028/). I have included the **<main>** tag from the draft HTML5.1 spec, and there is some preliminary ARIA support.

The DTD here does suffer from the fact that the HTML5 spec cannot be fully expressed as a DTD. However, it works just fine for well-formed XHTML5 (that is, HTML5 expressed as XML).

I have also included the official W3C DTDs for HTML4.01, XHTML 1.0, XHTML 1.1 plus the associated entity files.

### How do I get set up? ###

If you call DOMDocument::loadXML( $xhtmlFile ) with the LIBXML_DTDLOAD and LIBXML_DTDVALID flags, I noticed that a XHTML 1.0 file can take several seconds to load.  It turns out that if the XHTML document uses a DOCTYPE with the default public and system identifiers, then loadXML() will download the full DTD from W3C. This is what takes the time.

Of course, HTML5 does not have a public DTD, there is nothing to download - and therefore nothing to validate its DOM against!

The solution is to load a copy of the relevant DTD from a local file on the web server.

To use the DTDs provided here, place the entire DTD folder in the root directory for a website and implement the following example code. This code determines the structure of the incoming DTD and replaces the system identifier with a path to the matching locally-held DTD:

```
#!php

<?php

/**
 * Fix self closed tags that should have an end tag
 *
 * @author mjaque at ilkebenson dot com
 * @example <script/> to <script></script>
 *         
 * @param string $tag        	
 * @param string $xml        	
 * @return string
 */
function fixSelfClosedTags($tag, $xml) {
	$indice = 0;
	while ( $indice < strlen ( $xml ) ) {
		$pos = strpos ( $xml, "<$tag ", $indice );
		if ($pos) {
			$posClosedTag = strpos ( $xml, ">", $pos );
			if ($xml [$posClosedTag - 1] == "/") {
				$xml = substr_replace ( $xml, "></$tag>", $posClosedTag - 1, 2 );
			}
			$indice = $posClosedTag;
		} else
			break;
	}
	return $xml;
}

// Load the XHTML 'template' file you want to work with
$templateDocument = file_get_contents ( $templateFilePath );

// Determine the DOCTYPE of the page template
$theDocType = "";
$theDTD = "";
$match = array ();

if (stripos ( $templateDocument, '<!DOCTYPE html>' ) !== false) {
	$theDocType = "html5";
	$theDTD = "";
} else {
	// It's not an HTML5 document, so look for W3C DOCTYPE
	if (preg_match ( '#(?<=<!DOCTYPE HTML PUBLIC "-//W3C//DTD )[^/]+#i', $templateDocument, $match ) == 1) {
		$doctypeParts = explode ( ' ', $match [0] );
		array_walk ( $doctypeParts, 'trim' );
		if ((strtolower ( $doctypeParts [0] ) == 'html') && ($doctypeParts [1] == '4.01')) {
			$theDocType = 'html4';
		} elseif ((strtolower ( $doctypeParts [0] ) == 'xhtml') && ($doctypeParts [1] == '1.0')) {
			$theDocType = 'xhtml1';
		} elseif ((strtolower ( $doctypeParts [0] ) == 'xhtml') && ($doctypeParts [1] == '1.1')) {
			$theDocType = 'xhtml11';
		} else {
			die ( 'Unrecornized DOCTYPE for page template' );
		}
		
		// Which type?
		if (isset ( $doctypeParts [2] )) {
			if (in_array ( strtolower ( $doctypeParts [2] ), array (
					'',
					"strict" 
			) )) {
				$theDTD = "strict";
			}
			if (strtolower ( $doctypeParts [2] ) == "transitional") {
				$theDTD = "transitional";
			}
			if (strtolower ( $doctypeParts [2] ) == 'frameset') {
				die ( 'Frameset DOCTYPE not supported for template' );
			}
		}
		
		unset ( $doctypeParts, $match );
	} else {
		// No DOCTYPE at all, so assume HTML Transitional
		error_log ( 'No DOCTYPE set for template. Assuming HTML4 Transitional.' );
		$theDocType = "html4";
		$theDTD = "transitional";
	}
}

// Determine the external and local DTDs for the template document

if (empty ( $_SERVER ['SERVER_NAME'] ) && empty ( $_SERVER ['SERVER_PORT'] )) {
	die ( 'SERVER_NAME and SERVER_PORT cannot be determined.' );
}

$protocol = 'http://';
$port = ":" . filter_input ( INPUT_SERVER, 'SERVER_PORT', FILTER_SANITIZE_NUMBER_INT );

if (isset ( $_SERVER ["HTTPS"] ) && ($_SERVER ["HTTPS"] == 'on')) {
	$protocol = 'https://';
	$port = ''; // We don't need to set the port for HTTPS
}

$publicIdentifier = "";
$externalDTD = "";
$localDTD = "$protocol" . filter_input ( INPUT_SERVER, 'SERVER_NAME', FILTER_SANITIZE_STRIPPED ) . "$port/DTD/";

unset ( $protocol, $port );

switch ($theDocType) {
	case 'html4' :
		switch ($theDTD) {
			case 'strict' :
				$publicIdentifier = "-//W3C//DTD HTML 4.01//EN";
				$externalDTD .= 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd';
				$localDTD .= 'html4-strict.dtd';
				break;
			case 'transitional' :
				$publicIdentifier = "-//W3C//DTD HTML 4.01 Transitional//EN";
				$externalDTD .= 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd';
				$localDTD .= 'html4-transitional.dtd';
				break;
		}
		break;
	case 'html5' :
		$publicIdentifier = "about:legacy-compat";
		$localDTD .= 'html5.dtd';
		break;
	case 'xhtml1' :
		switch ($theDTD) {
			case 'strict' :
				$publicIdentifier = "-//W3C//DTD XHTML 1.0 Strict//EN";
				$externalDTD .= 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd';
				$localDTD .= 'xhtml1-strict.dtd';
				break;
			case 'transitional' :
				$publicIdentifier = "-//W3C//DTD XHTML 1.0 Transitional//EN";
				$externalDTD .= 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd';
				$localDTD .= 'xhtml1-transitional.dtd';
				break;
		}
		break;
	case 'xhtml11' :
		$publicIdentifier = '-//W3C//DTD XHTML 1.1//EN';
		$externalDTD = 'http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd';
		$localDTD .= 'xhtml11.dtd';
		break;
}

// Creates a DOMDocument instance
$doc = new DOMDocument ();
$doc->encoding = 'UTF-8';
$doc->standalone = false;
$doc->substituteEntities = false;
$doc->preserveWhiteSpace = false;
$doc->formatOutput = true;

// Load the incoming template, replacing the external DOCTYPE with a local one
$doctype = '<!DOCTYPE html PUBLIC "' . $publicIdentifier . '" "' . $localDTD . '">';
$doc->loadXML ( preg_replace ( '/^<!DOCTYPE.+?>/i', $doctype, $templateDocument ), LIBXML_COMPACT | LIBXML_NOXMLDECL | LIBXML_DTDLOAD | LIBXML_DTDVALID );

unset ( $templateDocument, $doctype );


// === DO WHAT YOU NEED TO DO WITH THE DOMDocument HERE ==//


// Now output the DOMDocument contents as XHTML5

$xhtmlOutput = $doc->saveXML ();

$xhtmlOutput = fixSelfClosedTags ( 'script', $xhtmlOutput );
$xhtmlOutput = fixSelfClosedTags ( 'ul', $xhtmlOutput );
$xhtmlOutput = fixSelfClosedTags ( 'div', $xhtmlOutput );

// Remove the XML headers to prevent IE6 from using Quirks mode
$xhtmlOutput = preg_replace ( '/<\?xml.*\?>\n/', '', $xhtmlOutput, 1 );

// Replace the local DOCTYPE with the official external one
if ($theDocType == 'html5') {
	$doctype = '<!DOCTYPE html>';
} else {
	$doctype = '<!DOCTYPE html PUBLIC "' . $publicIdentifier . '" "' . $externalDTD . '">';
}
$xhtmlOutput = preg_replace ( '/^<!DOCTYPE.+?>/', $doctype, $xhtmlOutput );

// Remove the Byte-Order-Mark (BOM)
if (strpos ( $xhtmlOutput, pack ( "CCC", 0xef, 0xbb, 0xbf ) ) !== false) {
	$xhtmlOutput = substr ( $xhtmlOutput, 3 );
}

header('Content-type: text/html; charset=utf-8');
echo $xhtmlOutput;

?>

```

### Got issues or ideas for improvements? ###

I'm no DTD expert, so I doubt the html5.dtd available here follows best practises. If you have issue and/or ideas for improving the HTML5 DTD, please open a ticket using the "Issues" menu here on BitBucket. Thanks!